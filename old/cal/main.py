import numpy as np
import urllib.parse
import urllib.request
from html.parser import HTMLParser as hp
from bs4 import BeautifulSoup as bs
import os.path as op
import json


def loadsite(url,values=None):
  if op.isfile("test.npz"):
    f=np.load("test.npz")
    if f["url"][0]==url:
      return f["q"][0]
  # print(data)
  if values==None:
    req = urllib.request.Request(url)
  else:
    data = urllib.parse.urlencode(values)
    req = urllib.request.Request(url, data.encode("utf-8"))
  response = urllib.request.urlopen(req)
  the_page = response.read()
  ret= the_page.decode("utf-8",errors="ignore")
  np.savez_compressed("test",url=[url],q=[ret])
  return ret

t1="<!-- %sresultblock% //-->"
t2="<!-- %eresultblock% //-->"
t3="cursor:pointer;"

def search(q):
  s=loadsite("https://fddb.mobi/search/?lang=de&cat=mobile-de&search="+q)
  if s.find(t1)<0:return ""
  s=s[s.find(t1)+len(t1):]
  if s.find(t2)<0:return ""
  s=s[:s.find(t2)]
  
  q=bs(s,features="html.parser")
  
  # q=q.find("h3")
  # validity=(q.text)#kinda useless i guess
  
  for d in q.find_all('div',style=True):
    if not d["style"].find(t3)>-1:continue
    lista=d.find("a",href=True)
    return (lista["href"])
  return ""

print(search("Mehl"))

