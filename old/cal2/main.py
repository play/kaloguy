import numpy as np
import urllib.parse
import urllib.request
from html.parser import HTMLParser as hp
from bs4 import BeautifulSoup as bs
import os.path as op
import json


def loadsite(url,values=None):
  if op.isfile("test.npz"):
    f=np.load("test.npz")
    if f["url"][0]==url:
      return f["q"][0]
  # print(data)
  if values==None:
    req = urllib.request.Request(url)
  else:
    data = urllib.parse.urlencode(values)
    req = urllib.request.Request(url, data.encode("utf-8"))
  response = urllib.request.urlopen(req)
  the_page = response.read()
  ret= the_page.decode("utf-8",errors="ignore")
  np.savez_compressed("test",url=[url],q=[ret])
  return ret

t1="Brennwert"
t2="</tr>"
t3="<td>"
t4="</td>"
t5=" "
def ana(q):
  s=loadsite(q)
  
  if s.find(t1)<0:return 0
  s=s[s.find(t1)+len(t1):]
  if s.find(t2)<0:return 0
  s=s[:s.find(t2)]
  if s.find(t3)<0:return 0
  s=s[s.find(t3)+len(t3):]
  if s.find(t4)<0:return 0
  s=s[:s.find(t4)]
  
  s=s.strip()
  if s.find(t5)<0:return 0
  s=s[:s.find(t5)]
  if not s.isnumeric():return 0
  return int(s)
  

print(ana("https://fddb.mobi/de/durchschnittswert_mehl_weizenmehl_typ_405.html"))

