import numpy as np
import urllib.parse
import urllib.request
from html.parser import HTMLParser as hp
from bs4 import BeautifulSoup as bs
import os.path as op
import json


def loadsite(url,values=None):

  # print(data)
  if values==None:
    req = urllib.request.Request(url)
  else:
    data = urllib.parse.urlencode(values)
    req = urllib.request.Request(url, data.encode("utf-8"))
  response = urllib.request.urlopen(req)
  the_page = response.read()
  return the_page.decode("utf-8")

ts="https://www.chefkoch.de/rezepte/1115981217686176/Blaubeerpfannkuchen-mit-Sosse.html"

if op.isfile("test.npz"):
  q=np.load("test.npz")["s"][0]
else:
  q=loadsite(ts)
  np.savez_compressed("test",s=[q])


# pp=bs(q)
# print(q.find("ü"))
# print(pp.text.find("ü"))
# exit()


# q='<a href="http://example.com/elsie" class="sister" id="link1">Elsie</a>'




tests='table class="ingredients table-header"'
tests2="</table>"

stris=[]
while q.find(tests)>-1:
  q=q[q.find(tests)+len(tests):]
  if q.find(tests2)<0:break
  stris.append(q[:q.find(tests2)])
  q=q[q.find(tests2):]
  

t3="<tbody>"
t4="</tbody>"
t5="                    "


masses=[]
units=[]
whats=[]


for s in stris:
  if s.find(t3)<0:continue
  s=s[s.find(t3)+len(t3):]
  if s.find(t4)<0:continue
  s=s[:s.find(t4)+len(t4)]
  
  p=bs(s,features="html.parser")
  for tr in p.find_all("tr"):
    mass,what=tr.find_all("td")
    mass=(mass.text).strip()
    what=(what.text).strip()
    print(mass,what)
    
    if mass.find(t5)<0:
      if len(mass)==0:
        masses.append("?")
      else:
        masses.append(mass)
      units.append("")
    else:
      unit=mass[mass.find(t5):].strip()
      mass=mass[:mass.find(t5)].strip()
      masses.append(mass)
      units.append(unit)
      
    whats.append(what)

recipe={"mass":masses,"unit":units,"what":whats}

print(recipe)
exit()

with open("test.json","w") as f:
  f.write(json.dumps(recipe))













