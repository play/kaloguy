import numpy as np
import urllib.parse
import urllib.request
from html.parser import HTMLParser as hp
from bs4 import BeautifulSoup as bs
import os.path as op
import json
from html import escape
# from conv import *




def loadsite(url,values=None):
  # print(data)
  if values==None:
    req = urllib.request.Request(url)
  else:
    data = urllib.parse.urlencode(values)
    req = urllib.request.Request(url, data.encode("utf-8"))
  response = urllib.request.urlopen(req)
  the_page = response.read()
  ret= the_page.decode("utf-8",errors="ignore")
  return ret


q="https://app.mensaplan.de/api/11101/de.mensaplan.app.android.aachen/aah8.json"

s=np.load("zw.npz")["q"][0]

j=json.loads(s)

day=0

#just for now
day+=1

j=j["days"]
j=j[day]

j=j["categories"]

trenn="\xa0"

r=[]

for cat in j:
  category=cat["name"]
  for meal in cat["meals"]:
    try:
      name=meal["name"]
      nut=meal["nutrition"]
      nut[1]=nut[1][5:]
      nut[2]=nut[2][3:]
      nut[3]=nut[3][3:]
      bw=float(nut[0][:nut[0].find(trenn)].replace(",","."))#brennwert
      fett=float(nut[1][:nut[1].find(trenn)].replace(",","."))#fett
      kohlenhydrate=float(nut[2][:nut[2].find(trenn)].replace(",","."))#kohlenhydrate
      eiweiß=float(nut[3][:nut[3].find(trenn)].replace(",","."))#eiweiß
      data={"cat":category,"name":name,"eiweiß":eiweiß,"fett":fett,"kohlenhydrate":kohlenhydrate,"kal":bw}
      r.append(data)
    except:pass
    
print(json.dumps(r,indent=2))
  