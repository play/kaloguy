#for 100g/ml in kcal
#Lebensmittel	Eiweiß	Fett	Kohlenhydrate	davon Zucker	Ballaststoffe	Alkohol	Kalorien	Energiedichte


import numpy as np
import matplotlib.pyplot as plt



fil="data.txt"

d=[]

with open(fil,"r") as f:
  for line in f.readlines():
    line=line.rstrip()
    lc=line.split("\t")
    nam,eiweiß,fett,kohlenhydrate,zucker,ballast,alkohol,kal,density=lc
    data={"name":nam,"eiweiß":eiweiß,"fett":fett,"kohlenhydrate":kohlenhydrate,"zucker":zucker,"ballaststoffe":ballast,"alkohol":alkohol,"kal":kal,"energydensity":density}
    d.append(data)

np.savez_compressed("data",q=d)
