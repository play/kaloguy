import numpy as np
import urllib.parse
import urllib.request
from html.parser import HTMLParser as hp
from bs4 import BeautifulSoup as bs
import os.path as op
import json
from html import escape
# from conv import *




def loadsite(url,values=None):
  # print(data)
  if values==None:
    req = urllib.request.Request(url)
  else:
    data = urllib.parse.urlencode(values)
    req = urllib.request.Request(url, data.encode("utf-8"))
  response = urllib.request.urlopen(req)
  the_page = response.read()
  ret= the_page.decode("utf-8",errors="ignore")
  return ret






def search(q,p=0):
  t1="<!-- %sresultblock% //-->"
  t2="<!-- %eresultblock% //-->"
  t3="cursor:pointer;"

  s=loadsite("https://fddb.mobi/search/?lang=de&cat=mobile-de&search="+q +"&p="+str(p))
  if s.find(t1)<0:return ""
  s=s[s.find(t1)+len(t1):]
  if s.find(t2)<0:return ""
  s=s[:s.find(t2)]
  
  q=bs(s,features="html.parser")
  
  # q=q.find("h3")
  # validity=(q.text)#kinda useless i guess
  
  ret=[]
  
  for d in q.find_all('div',style=True):
    if not d["style"].find(t3)>-1:continue
    lista=d.find("a",href=True)
    ret.append(lista["href"])
  return ret


def ana(q):
  t1="Brennwert"
  t2="</tr>"
  t3="<td>"
  t4="</td>"
  t5=" "
  s=loadsite(q)
  
  if s.find(t1)<0:return -1
  s=s[s.find(t1)+len(t1):]
  if s.find(t2)<0:return -1
  s=s[:s.find(t2)]
  if s.find(t3)<0:return -1
  s=s[s.find(t3)+len(t3):]
  if s.find(t4)<0:return -1
  s=s[:s.find(t4)]
  
  s=s.strip()
  if s.find(t5)<0:return -1
  s=s[:s.find(t5)]
  if not s.isnumeric():return -1
  return int(s)

def myenc(q):
  q=q.replace(" ","+")
  q=q.replace("ü","ue")
  q=q.replace("Ü","Ue")
  q=q.replace("ä","ae")
  q=q.replace("Ä","Ae")
  q=q.replace("ö","oe")
  q=q.replace("Ö","Oe")
  q=q.replace("ß","ss")
  return q
  
def getcal(q):
  q=myenc(q)
  print("searching for",q)
  l=search(q)
  if len(l)<1:return -1
  return ana(l)
  
def searchall(q):
  ret=[]
  start=0
  while True:
    ac=search(q,start)
    print("did",q,start)
    start+=10
    for zw in ac:
      ret.append(zw)
    if len(ac)<10:break
  return ret
def searchsall():
  q="abcdefghijklmnopqrstuvwxyz"
  ret=[]
  for ac in q:
    for zw in searchall(ac):
      ret.append(zw)
    np.savez_compressed("links",q=ret)
  

q=searchsall()







  
  