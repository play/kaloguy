import numpy as np
import urllib.parse
import urllib.request
from html.parser import HTMLParser as hp
from bs4 import BeautifulSoup as bs
import os.path as op
import json
from html import escape
# from conv import *




def loadsite(url,values=None):
  # print(data)
  if values==None:
    req = urllib.request.Request(url)
  else:
    data = urllib.parse.urlencode(values)
    req = urllib.request.Request(url, data.encode("utf-8"))
  response = urllib.request.urlopen(req)
  the_page = response.read()
  ret= the_page.decode("utf-8",errors="ignore")
  return ret


def getnam(s):
  t1="<h3>"
  t2="</h3>"
  s=s[s.find(t1)+len(t1):]
  s=s[:s.find(t2)]
  return s

def xan(s,t1):
  t2="</tr>"
  t3="<td>"
  t4="</td>"
  t5=" "
  
  if s.find(t1)<0:return -1
  s=s[s.find(t1)+len(t1):]
  if s.find(t2)<0:return -1
  s=s[:s.find(t2)]
  if s.find(t3)<0:return -1
  s=s[s.find(t3)+len(t3):]
  if s.find(t4)<0:return -1
  s=s[:s.find(t4)]
  
  s=s.strip()
  if s.find(t5)<0:return -1
  s=s[:s.find(t5)]
  s=s.replace(",",".")
  if not s.replace(".","").isnumeric():return -1
  return float(s)

def ana(q):
  s=loadsite(q)
  
  nam=getnam(s)
  
  t1="Brennwert"
  t2="Protein"
  t3="Kohlenhydrate"
  t4=">Fett"
  
  Brennwert=xan(s,t1)
  Protein=xan(s,t2)
  Kohlenhydrate=xan(s,t3)
  Fett=xan(s,t4)
  
  return nam,Brennwert,Protein,Kohlenhydrate,Fett

def myenc(q):
  q=q.replace(" ","+")
  q=q.replace("ü","ue")
  q=q.replace("Ü","Ue")
  q=q.replace("ä","ae")
  q=q.replace("Ä","Ae")
  q=q.replace("ö","oe")
  q=q.replace("Ö","Oe")
  q=q.replace("ß","ss")
  return q
  


def gex(q):
  nam,Brennwert,Protein,Kohlenhydrate,Fett=ana(q)
  Brennwert*=0.239#to make kJ in kcal
  ret={"name":nam,"fett":Fett,"kohlenhydrate":Kohlenhydrate,"protein":Protein,"kal":Brennwert,"link":q}
  return ret

def filter(q):
  return list(set(q))

links=np.load("links.npz")["q"]
print("found",len(links),"links")
links=filter(links)
print("filtered down to",len(links),"links")


ret=[]
llinks=len(links)
for link in links:
  ac=gex(link)
  print("did",len(ret),llinks,ac["name"])
  ret.append(ac)
  if len(ret)%100==0:
    print("saving..")
    np.savez_compressed("data",q=ret)
    print("saved")

print("saving..")
np.savez_compressed("data",q=ret)
print("saved")

print("done")



  
  