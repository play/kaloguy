import numpy as np
import urllib.parse
import urllib.request
from html.parser import HTMLParser as hp
from bs4 import BeautifulSoup as bs
import os.path as op
import json
from html import escape
from conv import *


link="https://www.chefkoch.de/rezepte/1115981217686176/Blaubeerpfannkuchen-mit-Sosse.html"

print("please enter link:")
link=input()


def loadsite(url,values=None):
  # print(data)
  if values==None:
    req = urllib.request.Request(url)
  else:
    data = urllib.parse.urlencode(values)
    req = urllib.request.Request(url, data.encode("utf-8"))
  response = urllib.request.urlopen(req)
  the_page = response.read()
  ret= the_page.decode("utf-8",errors="ignore")
  return ret






def search(q):
  t1="<!-- %sresultblock% //-->"
  t2="<!-- %eresultblock% //-->"
  t3="cursor:pointer;"

  s=loadsite("https://fddb.mobi/search/?lang=de&cat=mobile-de&search="+q)
  if s.find(t1)<0:return ""
  s=s[s.find(t1)+len(t1):]
  if s.find(t2)<0:return ""
  s=s[:s.find(t2)]
  
  q=bs(s,features="html.parser")
  
  # q=q.find("h3")
  # validity=(q.text)#kinda useless i guess
  
  for d in q.find_all('div',style=True):
    if not d["style"].find(t3)>-1:continue
    lista=d.find("a",href=True)
    return (lista["href"])
  return ""


def ana(q):
  t1="Brennwert"
  t2="</tr>"
  t3="<td>"
  t4="</td>"
  t5=" "
  s=loadsite(q)
  
  if s.find(t1)<0:return -1
  s=s[s.find(t1)+len(t1):]
  if s.find(t2)<0:return -1
  s=s[:s.find(t2)]
  if s.find(t3)<0:return -1
  s=s[s.find(t3)+len(t3):]
  if s.find(t4)<0:return -1
  s=s[:s.find(t4)]
  
  s=s.strip()
  if s.find(t5)<0:return -1
  s=s[:s.find(t5)]
  if not s.isnumeric():return -1
  return int(s)

def myenc(q):
  q=q.replace(" ","+")
  q=q.replace("ü","ue")
  q=q.replace("Ü","Ue")
  q=q.replace("ä","ae")
  q=q.replace("Ä","Ae")
  q=q.replace("ö","oe")
  q=q.replace("Ö","Oe")
  q=q.replace("ß","ss")
  return q
  
def getcal(q):
  q=myenc(q)
  print("searching for",q)
  l=search(q)
  if len(l)<1:return -1
  return ana(l)
  


def getrecipe(l):
  q=loadsite(l)
  tests='table class="ingredients table-header"'
  tests2="</table>"

  stris=[]
  while q.find(tests)>-1:
    q=q[q.find(tests)+len(tests):]
    if q.find(tests2)<0:break
    stris.append(q[:q.find(tests2)])
    q=q[q.find(tests2):]
    

  t3="<tbody>"
  t4="</tbody>"
  t5="                    "


  masses=[]
  units=[]
  whats=[]


  for s in stris:
    if s.find(t3)<0:continue
    s=s[s.find(t3)+len(t3):]
    if s.find(t4)<0:continue
    s=s[:s.find(t4)+len(t4)]
    
    p=bs(s,features="html.parser")
    for tr in p.find_all("tr"):
      mass,what=tr.find_all("td")
      mass=(mass.text).strip()
      what=(what.text).strip()
      print(mass,what)
      
      if mass.find(t5)<0:
        if len(mass)==0:
          masses.append("?")
        else:
          masses.append(mass)
        units.append("")
      else:
        unit=mass[mass.find(t5):].strip()
        mass=mass[:mass.find(t5)].strip()
        masses.append(mass)
        units.append(unit)
        
      whats.append(what)

  recipe={"mass":masses,"unit":units,"what":whats}
  return recipe


def calrep(q):
  r=getrecipe(q)
  cals=[]
  for w in r["what"]:
    try:
      cals.append(getcal(w))
    except:
      print("failed at",w,"setting to 0")
      cals.append(0)
  r["cals"]=cals
  return r
  

print("Queriyng the Websites")
cr=calrep(link)

print("converting")
ncal=conv(cr)

print("")
print("")
print("")

print("found ingredients")

what=cr["what"]

total=0.0
for i in range(len(ncal)):
  print(what[i]+": "+str(ncal[i])+" kJ")
  total+=ncal[i]


print("")
print("")
print("in total:")
print(total,"kJ")
print(total*0.239,"kcal")
  
  









  
  