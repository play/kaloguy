import numpy as np
import json



with open("test.json","r") as f:
  q=json.loads(f.read())



def fromunit(u):
  u=u.lower().strip()
  if u=="g" or u=="ml":return 1
  if u=="kg" or u=="l" or u=="liter":return 1000
  return 0

def mint(q):
  if type(q) is int:return float(q)
  if type(q) is float:return q
  q=q.replace(chr(188),".25")
  q=q.replace(chr(189),".5")
  q=q.replace(chr(190),".75")
  q=q.replace(",",".")
  if q.find("?")>-1:return 0.0
  if len(q)==0:return 0.0
  if ord(q[0])==8539:return 0.0
  # print("!"+q+"!",q.isnumeric(),q=="?",len(q),ord(q[0]))
  try:
    return float(q)
  except:
    if len(q)==1:print(q,ord(q[0]))
    return 0.0



def conv(q):
  mass=q["mass"]
  unit=q["unit"]
  cals=q["cals"]

  ncal=[]



  for i in range(len(mass)):
    m=mint(mass[i])
    u=unit[i]
    c=mint(cals[i])
    f=fromunit(u)
    nm=f*m
    cal=nm*c/100#since cal always for 100g
    # print(m,u,c,f,nm,cal)
    # print(total,cal)
    ncal.append(cal)
  return ncal
